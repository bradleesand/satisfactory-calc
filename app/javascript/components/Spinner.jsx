import React from "react"

class Spinner extends React.Component {
    render() {
        // TODO get FA working
        return <i className='fas fa-spinner-third fa-spin'/>;
    }
}

export default Spinner
